var user = JSON.parse(localStorage.getItem("user"));

if (user)
  printUser(user);
else
  ask();

document.getElementById("reset").addEventListener("click", reset);


function ask() {
  var name = prompt("What is your name?");
  var age = +prompt("What is your age?");

  if (name && age) {
    var user = {
      name: name,
      age: age
    };

    localStorage.setItem("user", JSON.stringify(user));
    printUser(user);
  }
}

function printUser(user) {
  document.getElementById("username").innerText = user.name + " (" + user.age + ")";
}

function reset() {
  localStorage.removeItem("user");
  ask();
}
