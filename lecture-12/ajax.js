$("#send").on("click", sendRequest);

function sendRequest() {
  $.get({
    url: "/fe-2307/lecture-12/response.html",
    success: function (response) {
      $("#response").html(response);
    },
    error: function(err) {
      $("#response").html(err.responseText);
    }
  });
}
