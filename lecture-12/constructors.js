function Cat(name) {
  this.name = name;
  this.sayMeow = function() {
    alert(this.name + " says 'meow'");
    return this;
  }
}

var c = new Cat("Murzik");
var d = new Cat("Mishka");
var e = new Cat("Vaska");

c.sayMeow().sayMeow().sayMeow();
e.sayMeow();
