var http = require('http');
var url = require('url');
var fs = require('fs');

var PORT = 8080;

http.createServer(function (req, res) {
  var statusCode = 200;
  var filename = "";
  var query = url.parse(req.url, true);

  switch(query.pathname) {
    case "/":
      filename = "index.html";
      break;
    case "/hello-world":
      filename = "hello-world.html";
      break;
  }

  fs.readFile(filename, 'utf8', function(err, data) {
    if (err) {
      statusCode = 404;
      data = "Sorry, I don't know this URL :(";
    }

    res.writeHead(statusCode, {'Content-Type': 'text/html'});
    res.end(data);
  });
}).listen(PORT);

console.log('Server running on port ' + PORT);
