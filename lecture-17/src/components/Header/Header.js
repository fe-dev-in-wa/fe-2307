import React from "react";
import { NavLink } from "react-router-dom";

import "./Header.css";


export default function Header() {
  return (
    <header>
      <h1>React App</h1>
      <nav>
        <NavLink exact to="/">Home</NavLink>
        <NavLink to="/contact">Concat Us</NavLink>
        <NavLink to="/about">About Us</NavLink>
      </nav>
    </header>
  );
}
