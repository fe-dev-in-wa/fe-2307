import React from "react";

import "./ListView.css";


export default function ListView(props) {
  const items = props.items;
  const onDelete = props.onDelete;


  return (
    <ul>
      { items.map(item => {
        return (
          <li key={item}>
            {item}
            <button onClick={() => onDelete(item)}>×</button>
          </li>
        );
      }) }
    </ul>
  )
}
