import React from "react";

import "./ListForm.css";


export default function ListForm(props) {
  const { onAddItem, currentInput, onUpdateInput } = props;


  return (
    <form onSubmit={onSubmit}>
      <input type="text"
             placeholder="Добавьте пункт..."
             value={currentInput}
             onChange={(event) => {onUpdateInput(event.target.value)}}
      />
      <button type="submit">+</button>
    </form>
  );

  function onSubmit(event) {
    event.preventDefault();

    if (currentInput && currentInput.trim())
      onAddItem();
  }
}
