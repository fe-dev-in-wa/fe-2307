import React from "react";
import TodoList from "../TodoList/TodoList";


export default function Home() {
  const items = [
    "Проснуться",
    "Побриться",
    "Умыться"
  ];

  return (
    <main>
      <h1 className="highlighted">Hello world</h1>
      <h2>This is a home page</h2>
      <p>Lorem ipsum</p>
      <h1 className="highlighted">TO DO:</h1>

      <TodoList items={items} id="home-todo" />

      <h1 className="highlighted">TO DO 2:</h1>
      <TodoList items={items} id="home-todo-2" />
    </main>
  );
}
