function TodoListView(controller) {
  const $header = $("<h2>TODO list</h2>");
  const $list = $("<ul/>");
  const $input = $("<input type=\"text\" placeholder=\"Добавьте пункт...\">");
  const $button = $("<button type=\"submit\">+</button>");
  const $form = $("<form/>");

  renderContainer();


  this.render = () => {
    $list.empty();

    controller.forEach(item => {
      $list.append(`
<li>
  <span class="item-text">${item}</span>
  <button>×</button>
</li>
`)
    });
  };

  this.render();

  $list.on("click", "button", event => {
    const $li = $(event.target).parent();
    const txt = $li.children(".item-text").text();

    controller.deleteItemByText(txt);

    this.render();
  });

  $form.on("submit", event => {
    const item = $input.val();

    if (item.trim()) {
      controller.addItem(item);
      $input.val("");

      this.render();
    }

    event.preventDefault();
  });

  function renderContainer() {
    $form.append($input).append($button);
    $(document.body).append($header).append($list).append($form);
  }
}
