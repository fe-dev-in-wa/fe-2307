$(function() {
  const controller = new TodoListController("list-1", [
    "Сделать ДЗ",
    "Лечь спать"
  ]);

  new TodoListView(controller);

  const controller2 = new TodoListController("list-2", [
    "Проснуться",
    "Умыться"
  ]);

  new TodoListView(controller2);
});
