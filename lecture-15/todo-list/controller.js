function TodoListController(storageKey, defaultItems = []) {
  const items = localStorage.getItem(storageKey);

  this.model = items ? JSON.parse(items) : defaultItems;


  this.deleteItemByText = function(text) {
    const i = this.model.indexOf(text);
    this.model.splice(i, 1);
    this.save();
  };

  this.addItem = function(text) {
    this.model.push(text);
    this.save();
  };

  this.forEach = function(callback) {
    this.model.forEach(callback);
  };

  this.save = function() {
    localStorage.setItem(storageKey, JSON.stringify(this.model));
  }
}
