(function() {
  const button = document.getElementById("toggle-theme");

  let lightTheme = localStorage.getItem("lightTheme") === "true";

  if (lightTheme)
    document.body.classList.add("light");


  button.addEventListener("click", () => {
    document.body.classList.toggle("light");

    lightTheme = !lightTheme;

    localStorage.setItem("lightTheme", lightTheme);
  });
})();
